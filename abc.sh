#!/bin/sh

cp .env-example .env;
cp .cf-example .cf;

# ... # change env & api token

# source .env

# while IFS== read -r key value; do
#   printf -v "$key" %s "$value" && export "$key"
# done <.env

export $(xargs <.env-example)

echo $NGINX_HOST
echo $CERTBOT_EMAIL

if [ -z "$1" ]; then
    echo "Argument1 is empty"
else
    echo $1
    sed -i -e "s/$NGINX_HOST/$1/g" .env
    sed -i -e "s/$NGINX_HOST/$1/g" nginx-main.conf
    sed -i -e "s/$NGINX_HOST/$1/g" options-ssl.conf
    if [ -z "$2" ]; then
        echo "Argument2 is empty"
    else
        echo $2
        sed -i -e "s/$CERTBOT_EMAIL/$2/g" .env
    fi
fi

sudo docker volume create etc-letsencrypt;